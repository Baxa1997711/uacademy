import React, { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import { Container, Button, Dialog } from "@mui/material";
import styles from "../Header.module.scss";
import Logo from "../../../../../public/images/Logo.png";
import { PhoneIcon, CloseIcon } from "../../../svg.js";
import axios from 'axios'
import Modal from "../../Modal/Modal";

function Desktop() {
  const router = useRouter();
  const slug = router.query.id
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  // const handleClose = () => setOpen(false);

<<<<<<< HEAD
  const initialValues = {
    slug,
    name: '',
    number: ''
  }
  const [userData, setUserData] = useState(initialValues)
  
   const handleSubmit = (e) => {
    e.preventDefault();
    const slug = userData.slug
    const name = userData.name
    const number = userData.number
    const formData = {slug, name, number};
    console.log(JSON.stringify(formData))
    axios.post(`https://admin.uacademy.uz/api/students`, {
      data: {
        ...userData
      }
    })
    .then(res => {
      // console.log('success', res)
    }).catch(err => {
      console.log('errrrroooor', err)
    }).finally((item) => {
      setUserData(initialValues)
      handleClose(false)
    })
   }
=======
  // const initialValues = {
  //   name: '',
  //   number: ''
  // }
  // const [userData, setUserData] = useState(initialValues)
  
  //  const handleSubmit = (e) => {
  //   e.preventDefault();
  //   const name = userData.name
  //   const number = userData.number
  //   const formData = {name , number};
  //   console.log(JSON.stringify(formData))
  //   axios.post(`https://admin.uacademy.uz/api/students`, {
  //     data: {
  //       ...userData
  //     }
  //   })
  //   .then(res => {
  //     // console.log('success', res)
  //   }).catch(err => {
  //     console.log('errrrroooor', err)
  //   }).finally((item) => {
  //     setUserData(initialValues)
  //     handleClose(false)
  //   })
  //  }
>>>>>>> 1b8d54c7b3212f4ca6dccee587b050275ab99cb6

  return (
    <div className={styles.desktop_topbar}>
      <Container className="container">
        <div className={styles.desktop_topbar_content}>
          <Link href="/">
            <a className={styles.desktop_logo}>
              <Image src={Logo} alt="" />
            </a>
          </Link>
          {/* {router.query.id === undefined ? (
            <div className={styles.topbar_links}>
              <Link href="/#courses">
                <a className={styles.topbar_link}>Все курсы</a>
              </Link>
              <Link href="/#feedback">
                <a className={styles.topbar_link}>Отзывы</a>
              </Link>
              <Link href="/">
                <a className={styles.topbar_link}>Связаться с нами</a>
              </Link>
            </div>
          ) : (
            ""
          )} */}

          <div className={styles.contact_panel}>
            <a href="tel:+998 (71) 202-42-22">
              <PhoneIcon />
              <span>+998 (71) 202-42-22</span>
            </a>

            <Button
              onClick={handleOpen}
              className={styles.register_btn}
              disableRipple
            >
              Записаться
            </Button>
          </div>
        </div>
      </Container>

      <Modal open={open} handleClose={() => setOpen(false)} />

      {/* <Dialog open={open} onClose={handleClose} id="dialog">
        <div className={styles.dialog_content}>
          <button onClick={handleClose} className={styles.close_icon}>
            <CloseIcon />
          </button>

          <div className={styles.dialog_context}>
            <h2>Заполните форму, чтобы оформить заказ</h2>
            <p>
              Если у вас есть вопросы о формате или вы не знаете, что
              выбрать,оставьте свой номер и наши операторы вам перезвонят
            </p>

            <form action="" onSubmit={handleSubmit}>
              <input type="text" placeholder="Введите имя" onChange={(e) => setUserData({...userData, name: e.target.value})}/>
              <input type="text" placeholder="Введите телефон" onChange={(e) => setUserData({...userData, number: e.target.value})}/>
              <button >Записаться на курс</button>
            </form>
          </div>
        </div>
      </Dialog> */}
    </div>
  );
}

export default Desktop;
