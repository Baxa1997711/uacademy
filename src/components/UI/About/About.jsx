import { Container } from '@mui/material'
import useTranslation from 'next-translate/useTranslation'
import styles from './About.module.scss'
import Image from 'next/image'


export function About() {
  const { t } = useTranslation('about')
  
  return (
    <div className={styles.about}>
        About
    </div>  
  )
}
