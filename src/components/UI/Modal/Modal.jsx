import React, {useState} from "react";
import { Dialog } from "@mui/material";
import axios from "axios";

import styles from "./modal.module.scss";
import { CloseIcon } from "../../svg.js";

function Modal({ open, handleClose }) {
  const initialValues = {
    name: "",
    number: "",
  };

  const [userData, setUserData] = useState(initialValues);

  const handleSubmit = (e) => {
    e.preventDefault();
    const name = userData.name;
    const number = userData.number;
    const formData = { name, number };
    console.log(JSON.stringify(formData));
    axios
      .post(`https://admin.uacademy.uz/api/students`, {
        data: {
          ...userData,
        },
      })
      .then((res) => {
        // console.log('success', res)
      })
      .catch((err) => {
        console.log("errrrroooor", err);
      })
      .finally((item) => {
        setUserData(initialValues);
        handleClose()
      });
  };

  return (
    <Dialog open={open} onClose={() => handleClose()} id="dialog">
      <div className={styles.dialog_content}>
        <button onClick={() => handleClose()} className={styles.close_icon}>
          <CloseIcon />
        </button>

        <div className={styles.dialog_context}>
          <h2>Заполните форму, чтобы оформить заказ</h2>
          <p>
            Если у вас есть вопросы о формате или вы не знаете, что
            выбрать,оставьте свой номер и наши операторы вам перезвонят
          </p>

          <form action="" onSubmit={handleSubmit}>
            <input
              type="text"
              placeholder="Введите имя"
              onChange={(e) =>
                setUserData({ ...userData, name: e.target.value })
              }
            />
            <input
              type="text"
              placeholder="Введите телефон"
              onChange={(e) =>
                setUserData({ ...userData, number: e.target.value })
              }
            />
            <button>Записаться на курс</button>
          </form>
        </div>
      </div>
    </Dialog>
  );
}

export default Modal;
