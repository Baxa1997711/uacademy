import React from "react";
import { Container } from "@mui/material";

import styles from "./features.module.scss";
import {
  FeatureIcon1,
  FeatureIcon2,
  FeatureIcon3,
  FeatureIcon4,
} from "../../svg";

const data = [
  {
    id: 0,
    img: <FeatureIcon1 />,
    title: "Katta proektlarda tajribaga ega bolgan mentorlar",
    subtitle: "Mentorlarimiz 3 tadan kop bolgan loyihalarda ishtirok etishgan",
  },
  {
    id: 1,
    img: <FeatureIcon2 />,
    title: "O‘z ishini ustasi bolgan mutahassislar",
    subtitle: "Mentorlar hozirgi kunda katta kompaniyalarda oz mutahassisliklarida ishlab kelishadi",
  },
  {
    id: 2,
    img: <FeatureIcon3 />,
    title: "Sizga konikma olishingizga yordam beramiz",
    subtitle: "Bizning maqsadimiz bilim emas konikmaga ega bolagan mutahassislarni tayyorlash",
  },
  {
    id: 3,
    img: <FeatureIcon4 />,
    title: "15 tadan kop bolmagan gruppalar",
    subtitle: "O’quv gugurhlarimiz 15 tadan kop oquvchi oqitilmaydi",
  },
];

function Features() {
  return (
    <div className={styles.features}>
      <Container className="container">
        <h2 className="title">Почему выберают нас</h2>
        <div className={styles.features_content}>
          {data?.map((item) => (
            <div key={item?.id} className={styles.features_content_item}>
              <div className={styles.features_content_icon}>{item?.img}</div>
              <div className={styles.features_content_context}>
                <h4>{item?.title}</h4>
                <p>{item?.subtitle}</p>
              </div>
            </div>
          ))}
        </div>
      </Container>
    </div>
  );
}

export default Features;
