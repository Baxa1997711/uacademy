import React, { useState, useEffect } from "react";
import Image from "next/image";
import { Container, Box, Button, Dialog } from "@mui/material";
import styles from "./single.module.scss";
import { CloseIcon } from "../../../svg.js";
import axios from 'axios'
import { useRouter } from 'next/router'


function SingleBanner({ bannerInfo }) {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const router = useRouter();
  const slug = router.query.id
  
  const initialValues = {
    slug,
    name: '',
    number: ''
  }
  const [userData, setUserData] = useState(initialValues)
  
   const handleSubmit = (e) => {
    e.preventDefault();
    const slug = userData.slug
    const name = userData.name
    const number = userData.number
    const formData = {slug, name, number};
    console.log(JSON.stringify(formData))
    axios.post(`https://admin.uacademy.uz/api/students`, {
      data: {
        ...userData
      }
    })
    .then(res => {
      // console.log('success', res)
    }).catch(err => {
      console.log('errrrroooor', err)
    }).finally((item) => {
      setUserData(initialValues)
    })
   }

  
  
  // const [value, setValue] = useState()
  // console.log('value==========>',value)
  // const getData = () => {
  //   axios.get('https://admin.uacademy.uz/api/banners?populate=*')
  //   .then((res) => {
  //     setValue(res.data?.data)
  //   }).catch((err) => {
  //     console.log('single_banner error ====>', err)
  //   })
  // }
  // useEffect(() => {
  //   getData()
  // }, [])
  
  
  // console.log('router_id =========', value[0].attributes.title)
  
  // const front = value.find((item) => item.id === 2)
  // const backend = value.find((item) => item.id === 3)
  // const qa = value.find((item) => item.id === 4)
  // const android = value.find((item) => item.id === 5)
  // const devops = value.find((item) => item.id === 6)
  // const design = value.find((item) => item.id === 1)
  
  // const data = 
  //    router.query.id === 'Frontend-Developer' ? value[0].attributes 
  //  : router.query.id === 'Backend-Developer' ? 'backend'
  //  : router.query.id === 'QA' ? 'qa' 
  //  : router.query.id === 'Android-IOS-Developer' ? 'android' 
  //  : router.query.id === 'Devops' ? 'devops' 
  //  : router.query.id === 'Design' ? 'design' 
  //  : null
  
  // console.log(router.query.id === 'Frontend-Developer' ? 'done' : null)
  // console.log('data =====>', data)

  // router.query.id.includes("Frontend") && value ? value[0]?.attributes.title : ""
  return (
    <div className={styles.singleBanner}>
      <Container className="container">
        <div className={styles.banner_wrapper}>
          <Box>
            <h1 className={styles.banner_title}>{bannerInfo?.title}</h1>
            <p className={styles.banner_subtitle}>{bannerInfo?.subtitle}</p>
            <Button onClick={handleOpen} className={styles.banner_btn}>
              Подать заявку
            </Button>
          </Box>
          <Box>
            <Image
              src={bannerInfo?.bannerImage}
              alt=""
              width={650}
              height={400}
            />
          </Box>
        </div>

        <Dialog open={open} onClose={handleClose} id="dialog">
          <div className={styles.dialog_content}>
            <button onClick={handleClose} className={styles.close_icon}>
              <CloseIcon />
            </button>

            <div className={styles.dialog_context}>
              <h2>Заполните форму, чтобы оформить заказ</h2>
              <p>
                Если у вас есть вопросы о формате или вы не знаете, что
                выбрать,оставьте свой номер и наши операторы вам перезвонят
              </p>

              <form action="" onSubmit={handleSubmit}>
                <input type="text" placeholder="Введите имя" 
                value={userData?.name} 
                onChange={(e) => setUserData({...userData, name: e.target.value})}
                required
                />
                <input type="text" placeholder="Введите телефон" 
                value={userData?.number} 
                onChange={(e) => setUserData({...userData, number: e.target.value})}
                required
                />
                <button>Записаться на курс</button>
              </form>
            </div>
          </div>
        </Dialog>
      </Container>

      <Image
        src={bannerInfo?.bannerBg}
        priority={true}
        alt="cspace"
        layout="fill"
      />
    </div>
  );
}

export default SingleBanner;
