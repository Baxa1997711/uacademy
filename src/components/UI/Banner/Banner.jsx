import React from "react";
import Image from "next/image";
import Link from "next/link";
import { Container, Box } from "@mui/material";
import styles from "./banner.module.scss";
import BannerBg from "../../../../public/images/banner_bg.png";
import BannerImage from "../../../../public/images/banner_image.png";

function Banner() {
  return (
    <div className={styles.banner}>
      <Container className="container">
        <div className={styles.banner_content}>
          <div className={styles.banner_text}>
            <Box>
              <h1 className={styles.banner_title}>
                IT академия разработки и дизайна
              </h1>
              <p className={styles.banner_subtitle}>
                Мы даем современные практические навыки для карьерного рывка,
                роста бизнеса.
              </p>
              <Link href="#form">
                <a className={styles.banner_btn}>Подать заявку</a>
              </Link>
            </Box>
          </div>
          
          <div className={styles.banner_img}>
            <Image src={BannerImage} alt="" />
          </div>
        </div>
      </Container>

      <Image
        src={BannerBg}
        objectFit="cover"
        priority={true}
        alt="cspace"
        layout="fill"
      />
    </div>
  );
}

export default Banner;
