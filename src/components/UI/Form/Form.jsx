import React, {useState, useEffect} from "react";
import { Container } from "@mui/material";
import styles from "./form.module.scss";
import axios from 'axios'
import { useRouter } from 'next/router'


function Form() {
  
  const router = useRouter()
  const slug = router.query.id
    const initialValues = {
    slug,  
    name: '',
    number: ''
  }
  const [userData, setUserData] = useState(initialValues)
  
   const handleSubmit = (e) => {
    e.preventDefault();
    const slug = userData.slug
    const name = userData.name
    const number = userData.number
    const formData = {slug, name, number};
    console.log(JSON.stringify(formData))
    axios.post(`https://admin.uacademy.uz/api/students`, {
      data: {
        ...userData
      }
    })
    .then(res => {
      // console.log('success', res)
    }).catch(err => {
      console.log('errrrroooor', err)
    }).finally((item) => {
      setUserData(initialValues)
    })
   }


  return (
    <div className={styles.form} id="form">
      <Container className="container">
        <div className={styles.form_content}>
          <div className={styles.form_content_text}>
            <h2>Не можете выбрать направление?</h2>
            <p>
              Если у вас есть вопросы о формате или вы не знаете, что
              выбрать,оставьте свой номер и наши операторы вам перезвонят
            </p>
          </div>

          <form action="" className={styles.form_panel} onSubmit={handleSubmit}>
            <input 
            type="text"
             placeholder="Введите имя" 
             value={userData?.name} 
             onChange={(e) => setUserData({...userData, name: e.target.value})}
             required
             />
             
            <input 
            type="number" 
            placeholder="Введите телефон" 
            value={userData?.number} 
             onChange={(e) => setUserData({...userData, number: e.target.value})}
             required
            />
            <button type="submit">Отправить</button>
          </form>
        </div>
      </Container>
    </div>
  );
}

export default Form;
