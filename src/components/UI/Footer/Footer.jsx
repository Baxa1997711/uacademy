import Link from "next/link";
import Image from "next/image";
import { Container } from "@mui/material";
import useTranslation from "next-translate/useTranslation";

import styles from "./Footer.module.scss";
import Logo from "../../../../public/images/Logo.png";
import {
  GoogleIcon,
  WhatsupIcon,
  InstagramIcon,
  FacebookIcon,
} from "../../svg.js";

export function Footer() {
  const { t } = useTranslation("common");

  return (
    <div className={styles.footer}>
      <Container className="container">
        <div className={styles.footer_top}>
          <div className={styles.footer_logo}>
            <Link href="/">
              <a>
                <Image src={Logo} alt="" />
              </a>
            </Link>
          </div>
          <div className={styles.footer_links}>
            <Link href="/">
              <a>Все курсы</a>
            </Link>
            <Link href="/">
              <a>Отзывы</a>
            </Link>
            <Link href="/">
              <a>Связаться с нами</a>
            </Link>
          </div>
          <div className={styles.footer_contacts_container}>
            <div className={styles.footer_contacts_item}>
              <h4>+998 97 736 20 22</h4>
              <p>Контактний центр</p>
            </div>
            <div className={styles.footer_contacts_item}>
              <h4>Ташкент, Улица Чуст. 1 100077</h4>
              <p>Aдрес</p>
            </div>
          </div>
        </div>
        <div className={styles.footer_bottom}>
          <p className={styles.footer_copyright}>
            © Uacademy 2021 - 2022 All rights reserved
          </p>
          <div className={styles.footer_socials}>
            <a href="">
              <GoogleIcon />
            </a>
            <a href="">
              <WhatsupIcon />
            </a>
            <a href="">
              <InstagramIcon />
            </a>
            <a href="">
              <FacebookIcon />
            </a>
          </div>
        </div>
      </Container>
    </div>
  );
}
