import React, { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { Container } from "@mui/material";
import styles from "./courses.module.scss";
import axios from 'axios'


const data = [
  {
    id: 0,
    img: "/images/courses_img1.jpg",
    title: "Frontend-разработчик",
    subtitle:
      "Lorem ipsum dolor amet, consectetur adipiscing elit. Mattis et sed nam sem tellus erat.",
    slug: "Frontend-Developer",
  },
  {
    id: 1,
    img: "/images/courses_img2.jpg",
    title: "Backend разработчик",
    subtitle:
      "Lorem ipsum dolor amet, consectetur adipiscing elit. Mattis et sed nam sem tellus erat.",
    slug: "Backend-Developer",
  },
  {
    id: 2,
    img: "/images/courses_img3.jpg",
    title: "QA Тестировщик",
    subtitle:
      "Lorem ipsum dolor amet, consectetur adipiscing elit. Mattis et sed nam sem tellus erat.",
    slug: "QA",
  },
  {
    id: 3,
    img: "/images/courses_img4.jpg",
    title: "Android,iOS developer",
    subtitle:
      "Lorem ipsum dolor amet, consectetur adipiscing elit. Mattis et sed nam sem tellus erat.",
    slug: "Android-IOS-Developer",
  },
  {
    id: 4,
    img: "/images/courses_img5.jpg",
    title: "Devops",
    subtitle:
      "Lorem ipsum dolor amet, consectetur adipiscing elit. Mattis et sed nam sem tellus erat.",
    slug: "Devops",
  },
  {
    id: 5,
    img: "/images/courses_img6.jpg",
    title: "Ui/Ux Design",
    subtitle:
      "Lorem ipsum dolor amet, consectetur adipiscing elit. Mattis et sed nam sem tellus erat.",
    slug: "Design",
  },
];

function Courses() {
  const router = useRouter();

  const handleClick = (slug) => {
    router.push(`/${slug}`);
  };
  
  // const [value, setValue] = useState([]);
  // console.log('courses =>>>>', value)
  // const getData = () => {
  //   axios.get('https://admin.uacademy.uz/api/courses?populate=*')
  //   .then((res) => {
  //     setValue(res.data);
  //   }).catch((error) => {
  //     console.log('courses error ===>', error)
  //   })
  // }
  
  // useEffect(() => {
  //   getData();
  // }, [])
  

  return (
    <div className={styles.courses} id="courses">
      <Container className="container">
        <h2 className="title">Выберите свой курс</h2>
        <div className={styles.courses_content}>
          {data?.map((item) => (
            <div
              onClick={() => handleClick(item?.slug)}
              key={item?.id}
              className={styles.courses_content_item}
            >
              <div className={styles.courses_content_img}>
                <img src={item?.img} alt=""/>
              </div>
              <div className={styles.courses_content_context}>
                <h4>{item?.title}</h4>
                <p>{item?.subtitle}</p>
                <Link href={`${item?.slug}`}>
                  <a onClick={() => handleClick(item?.attributes?.slug)}>Узнать большее</a>
                </Link>
              </div>
            </div>
          ))}
        </div>
      </Container>
    </div>
  );
}

export default Courses;
