import { Container } from "@mui/material";
import React from "react";
import styles from "./advantages.module.scss";
import { AdvantageIcon1, AdvantageIcon2, AdvantageIcon3 } from "../../svg.js";
import advantages from "../../../../public/images/advantage_img.png";
import layer from "../../../../public/images/advantages_layer.png";
import Image from "next/image";

const data = [
  {
    id: 0,
    title: "80% практики",
    img: <AdvantageIcon1 />,
  },
  {
    id: 1,
    title: "Учителя с опытом работы в проектах",
    img: <AdvantageIcon2 />,
  },
  {
    id: 2,
    title: "Учебная программа современных технологий",
    img: <AdvantageIcon3 />,
  },
];

function Advantages(props) {
  return (
    <div className={styles.advantages}>
      <Container className="container">
        <div className={styles.advantages_content}>
          <div className={styles.advantages_content_item}>
            <div className={styles.advantages_text}>
              <h2>Наши преимушество</h2>
              <div className={styles.advantages_img_mobile} >
                <Image src={advantages} alt=""/>
              </div>
              <p>
                Для работы с любым из наших курсов, ученикам не потребуются
                начальные знания, все материалы ориентированы на людей с
                нулевыми знаниями в той или иной тематике.
              </p>
            </div>

            <div className={styles.advantages_context}>
              {data?.map((item) => (
                <div key={item?.id} className={styles.advantages_context_item}>
                  <div className={styles.advantages_context_info}>
                    <div className={styles.advantages_context_icon}>
                      {item?.img}
                    </div>
                    <p>{item?.title}</p>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={styles.advantages_img}>
          <Image src={advantages} alt="" />
        </div>
        <div className={styles.layer_img}>
          <Image src={layer} alt="" />
        </div>
      </Container>
    </div>
  );
}

export default Advantages;
